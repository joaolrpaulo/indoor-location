default: all

prepare_build:
	@mkdir -p build/bin
	@cp configs/properties.json build/bin/properties.json

build: prepare_build
	@cd build/ && conan install .. && cmake .. && make -j 16

all: build
	@cd build/bin && ./indoor_location