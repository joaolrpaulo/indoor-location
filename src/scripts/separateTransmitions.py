import csv
import math


def main():
    header = []
    new_array = []
    new_array_2 = []

    with open("../../data/CSV/solutionRange.csv", "rb") as csv_file:
        reader = csv.reader(csv_file, delimiter=",")

        for index, row in enumerate(reader):
            if index == 0:
                row.append("disReal")
                row.append("baset1")
                row.append("baset2")
                row.append("baset3")
                header = row
                continue

            if not "nan" in row[7]:
                x21 = (float(row[6]) - 0)**2
                y21 = (float(row[7]) - 190)**2
                c = math.sqrt(x21 + y21)

                row.append(c)
                row.append(int(row[1]) - 1539900448005801)
                row.append(int(row[2]) - 1539900448005489)
                row.append(int(row[3]) - 1539900448005801)

                new_array.append(row)

        for row in chunks(new_array, 4):
            for elem in row:
                new_array_2.append(elem)

    with open("../../data/CSV/solutionRangeReady.csv", 'w') as outcsv:
        # configure writer to write standard csv file
        writer = csv.writer(outcsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        writer.writerow(header)

        for item in new_array_2:
            # Write item to outcsv
            writer.writerow(item)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


if __name__ == "__main__":
    main()