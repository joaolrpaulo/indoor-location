import zmq
import math
from random import randint
import time

SPEED_OF_SOUND = 346.13

CM_PER_NANOSECOND = 0.00034613
CM_PER_MICROSECOND = 3.4613

def main():
    # generateExactPositions()

    generatePositionsTo3D()

    # h1, h2, h3 = getCenterPositionTimes()
    # print getCenterPositionTimes(), h1 - h2


def generatePositionsTo3D():
    t1, t2, t3 = getCenterPositionTimes()

    for i in range(-10000, 10000, 10):
        # for j in range(0, 10000, 100):
        time.sleep(0.05)

        sendData(1, t1)
        sendData(2, t2)
        sendData(3, t3 - i)

        # time.sleep(0.05)

        # sendData(1, t1)
        # sendData(2, t2)
        # sendData(3, t3 - i)

            # time.sleep(0.05)

            # sendData(1, t1 + i)
            # sendData(2, t2)
            # sendData(3, t3 - j)

            # time.sleep(0.05)

            # sendData(1, t1 - i)
            # sendData(2, t2)
            # sendData(3, t3 + j)

            # time.sleep(0.05)

            # sendData(1, t1 - i)
            # sendData(2, t2)
            # sendData(3, t3 - j)

    # t1, t2, t3 = getLeftPositionTimes()

    # for i in range(0, 10000, 100):
    #     for j in range(0, 10000, 100):
    #         time.sleep(0.1)

    #         sendData(1, t1)
    #         sendData(2, t2 + i)
    #         sendData(3, t3 + j)

    #         time.sleep(0.1)

    #         sendData(1, t1)
    #         sendData(2, t2 + i)
    #         sendData(3, t3 - j)

    #         time.sleep(0.1)

    #         sendData(1, t1)
    #         sendData(2, t2 - i)
    #         sendData(3, t3 + j)

    #         time.sleep(0.1)

    #         sendData(1, t1)
    #         sendData(2, t2 - i)
    #         sendData(3, t3 - j)

    # t1, t2, t3 = getRightPositionTimes()

    # for i in range(0, 10000, 100):
    #     for j in range(0, 10000, 100):
    #         sendData(1, t1 + i)
    #         sendData(2, t2 + j)
    #         sendData(3, t3)

    #         time.sleep(0.1)

    #         sendData(1, t1 + i)
    #         sendData(2, t2 - j)
    #         sendData(3, t3)

    #         time.sleep(0.1)

    #         sendData(1, t1 - i)
    #         sendData(2, t2 + j)
    #         sendData(3, t3)

    #         time.sleep(0.1)

    #         sendData(1, t1 - i)
    #         sendData(2, t2 - j)
    #         sendData(3, t3)


def generateExactPositions():
    t1, t2, t3 = getCenterPositionTimes()

    for i in range(-1000, 1000):
        time.sleep(0.1)
        sendData(1, t1)
        sendData(2, t2)
        sendData(3, t3 - i)

    # t1, t2, t3 = getLeftPositionTimes()
    #
    # sendData(1, t1)
    # sendData(2, t2)
    # sendData(3, t3 - 600)
    #
    #
    # t1, t2, t3 = getRightPositionTimes()
    #
    # sendData(1, t1 - 600)
    # sendData(2, t2)
    # sendData(3, t3)


def generateDataWithDataFromGnuradio():
    mean = 85069
    variance = 352676038660
    std = 593865

    t1, t2, t3 = getCenterPositionTimes()

    sendData(1, t1 - (mean))
    sendData(2, t2)
    sendData(3, t3 - (mean))

    sendData(1, t1 - (mean + std))
    sendData(2, t2)
    sendData(3, t3 - (mean + std))

    sendData(1, t1 - (mean + std * 2))
    sendData(2, t2)
    sendData(3, t3 - (mean + std * 2))

    t1, t2, t3 = getLeftPositionTimes()

    sendData(1, t1 - (mean))
    sendData(2, t2)
    sendData(3, t3 - (mean))

    sendData(1, t1 - (mean + std))
    sendData(2, t2)
    sendData(3, t3 - (mean + std))

    sendData(1, t1 - (mean + std * 2))
    sendData(2, t2)
    sendData(3, t3 - (mean + std * 2))

    t1, t2, t3 = getRightPositionTimes()

    sendData(1, t1 - (mean))
    sendData(2, t2)
    sendData(3, t3 - (mean))

    sendData(1, t1 - (mean + std))
    sendData(2, t2)
    sendData(3, t3 - (mean + std))

    sendData(1, t1 - (mean + std * 2))
    sendData(2, t2)
    sendData(3, t3 - (mean + std * 2))


def generateDataFromRandomInput():
    print "r1,r2,r3"

    # Center Position
    t1, t2, t3 = getCenterPositionTimes()

    for i in range(0, 25):
        r1 = randint(-60000, 60000)
        r2 = randint(-60000, 60000)
        r3 = randint(-60000, 60000)

        print "%d,%d,%d" % (r1, r2, r3)

        sendData(1, t1 + r1)
        sendData(2, t2 + r2)
        sendData(3, t3 + r3)

    print "0,0,0"    

    # Left Position
    t1, t2, t3 = getLeftPositionTimes()

    for i in range(0, 25):
        r1 = randint(-60000, 60000)
        r2 = randint(-60000, 60000)
        r3 = randint(-60000, 60000)

        print "%d,%d,%d" % (r1, r2, r3)

        sendData(1, t1 + r1)
        sendData(2, t2 + r2)
        sendData(3, t3 + r3)

    h1 = calcHypotenuse(190.0, 130.0)
    h2 = calcHypotenuse(190.0, 65.0)
    h3 = 190.0

    print "0,0,0"

    # Right Position
    t1, t2, t3 = getRightPositionTimes()

    for i in range(0, 25):
        r1 = randint(-60000, 60000)
        r2 = randint(-60000, 60000)
        r3 = randint(-60000, 60000)

        print "%d,%d,%d" % (r1, r2, r3)

        sendData(1, t1 + r1)
        sendData(2, t2 + r2)
        sendData(3, t3 + r3)


def getCenterPositionTimes():
    h1 = calcHypotenuse(190.0, 65.0)
    h2 = 190.0
    h3 = calcHypotenuse(190.0, 65.0)

    print h1, h2, h3

    return timestampFromDistance(h1, h2, h3)


def getLeftPositionTimes():
    h1 = 190.0
    h2 = calcHypotenuse(190.0, 65.0)
    h3 = calcHypotenuse(190.0, 130.0)

    return timestampFromDistance(h1, h2, h3)


def getRightPositionTimes():
    h1 = calcHypotenuse(190.0, 130.0)
    h2 = calcHypotenuse(190.0, 65.0)
    h3 = 190.0

    return timestampFromDistance(h1, h2, h3)


def timestampFromDistance(h1, h2, h3):
    tp1 = (h1 / 100) / SPEED_OF_SOUND * 1e6
    tp2 = (h2 / 100) / SPEED_OF_SOUND * 1e6
    tp3 = (h3 / 100) / SPEED_OF_SOUND * 1e6

    tp1 = int(tp1)
    tp2 = int(tp2)
    tp3 = int(tp3)

    tbase = 1539900448000000

    return tbase + tp1, tbase + tp2, tbase + tp3


def calcHypotenuse(height, width):
    return math.sqrt(height**2 + width**2)


def sendData(station, timestamp):
    context = zmq.Context()

    #  Socket to talk to server
    socket = context.socket(zmq.DEALER)
    socket.setsockopt(zmq.IDENTITY, b"%s" % (station))
    socket.connect("tcp://localhost:10001")

    socket.send("%s" % timestamp)

    socket.close()


if __name__ == "__main__":
    main()