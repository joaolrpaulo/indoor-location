#include "TrilaterationWithEWMA.h"
#include "armadillo"
#include "../Storage/EnvironmentalStorage.h"
#include "../../Config/ConfigParser.h"

/**
 * TrilaterationWithEMSA
 *
 * The algorithm used to find the position of the device
 * using TDOA transmittions.
 *
 * @param stations The registered stations.
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
void TrilaterationWithEMSA::init(std::vector<Station *> stations) {
    const auto speedOfSound = EnvironmentalStorage::getInstance()->getSpeedOfSound()["25"];

    s1 = findStation(stations, "1");
    s2 = findStation(stations, "2");
    s3 = findStation(stations, "3");

    double x1 = s1->getPosition()->getX();
    double x2 = s2->getPosition()->getX();
    double x3 = s3->getPosition()->getX();

    long tdoa21 = s2->getDeltaTime() - s1->getDeltaTime();
    long tdoa23 = s2->getDeltaTime() - s3->getDeltaTime();

    if (ConfigParser::getInstance()->isExponentialMovingAverageActive()) {
        if (diff21Avg == -1) {
            diff21Avg = tdoa21;
        } else {
            diff21Avg = long(alpha * tdoa21 + (1 - alpha) * diff21Avg);
        }

        if (diff23Avg == -1) {
            diff23Avg = tdoa23;
        } else {
            diff23Avg = long(alpha * tdoa23 + (1 - alpha) * diff23Avg);
        }
    } else {
        diff21Avg = tdoa21;
        diff23Avg = tdoa23;
    }

    double d21 = speedOfSound * diff21Avg;
    double d23 = speedOfSound * diff23Avg;

    arma::mat matrixA(2, 2);

    matrixA(0, 0) = x1 - x2;
    matrixA(1, 0) = x3 - x2;

    matrixA(0, 1) = d21;
    matrixA(1, 1) = d23;

    matrixA *= 2;

    arma::mat matrixB(2, 1);

    matrixB(0, 0) = (pow(x1, 2.0) - pow(x2, 2.0)) - pow(d21, 2.0);
    matrixB(1, 0) = (pow(x3, 2.0) - pow(x2, 2.0)) - pow(d23, 2.0);

    arma::mat matrixX = arma::solve(matrixA, matrixB);

    double xm = matrixX(0, 0);
    double d2 = matrixX(1, 0);

    double zm = sqrt(pow(d2, 2.0) - pow(xm - x2, 2.0));

    SQLiteStorage::getInstance()->persistTrilaterationResult(
            std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count(),
            s1->getDeltaTime(),
            s2->getDeltaTime(),
            s3->getDeltaTime(),
            diff21Avg,
            diff23Avg,
            xm * 100,
            zm * 100,
            std::abs(d2) * 100
    );
}

Station* TrilaterationWithEMSA::findStation(std::vector<Station *> stations, std::string id) {
    return std::find_if(stations.begin(), stations.end(), [=](const Station *station) {
        return station->getStationID() == id;
    }).operator*();
}