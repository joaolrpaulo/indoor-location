#ifndef INDOOR_LOCATION_TRILATERATIONWITHEMSA_H
#define INDOOR_LOCATION_TRILATERATIONWITHEMSA_H


#include <spdlog/logger.h>
#include <spdlog/spdlog.h>
#include "AbstractMultiAlgorithm.h"

class TrilaterationWithEMSA : public AbstractMultiAlgorithm {
private:
    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("SimpleTrilateration");

    Station* findStation(std::vector<Station *> stations, std::string id);
public:
    void init(std::vector<Station*>) override;

    // TDOA's with exponential mean square averages
    long diff21Avg = -1, diff23Avg = -1;

    Station *s1, *s2, *s3;

    double alpha = 0.25;
};

#endif //INDOOR_LOCATION_TRILATERATIONWITHEMSA_H
