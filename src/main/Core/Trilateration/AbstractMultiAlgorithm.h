#ifndef INDOOR_LOCATION_ABSTRACTMULTIALGORITHM_H
#define INDOOR_LOCATION_ABSTRACTMULTIALGORITHM_H


#include "../../Types/Station.h"

class AbstractMultiAlgorithm {
    public:
        virtual void init(std::vector<Station *> stations) = 0;
};

#endif //INDOOR_LOCATION_ABSTRACTMULTIALGORITHM_H
