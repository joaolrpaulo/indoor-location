#ifndef INDOOR_LOCATION_CPP_TRILATERATIONMANAGER_H
#define INDOOR_LOCATION_CPP_TRILATERATIONMANAGER_H

#include <spdlog/spdlog.h>
#include "AbstractMultiAlgorithm.h"
#include "../Storage/StationStorage.h"
#include "../Storage/EnvironmentalStorage.h"

class TrilaterationManager {
private:
    static TrilaterationManager* instance;
    TrilaterationManager(AbstractMultiAlgorithm*);

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("TrilaterationManager");

    StationStorage* stationStorage;
    EnvironmentalStorage* environmentalStorage;

    AbstractMultiAlgorithm* algorithm;
public:
    static TrilaterationManager* getInstance(AbstractMultiAlgorithm*);

    void find();
};

#endif //INDOOR_LOCATION_CPP_TRILATERATIONMANAGER_H
