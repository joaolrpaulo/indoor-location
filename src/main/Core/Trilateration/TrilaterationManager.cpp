/**
 * TrilaterationManager
 *
 * The class responsible for every trilateration calculations.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */

#include <iostream>
#include <spdlog/spdlog.h>
#include "TrilaterationManager.h"
#include "TrilaterationWithEWMA.h"

TrilaterationManager* TrilaterationManager::instance = nullptr;

TrilaterationManager* TrilaterationManager::getInstance(AbstractMultiAlgorithm* algorithm) {
    if (instance == nullptr) {
        if (algorithm == nullptr) {
            algorithm = new TrilaterationWithEMSA();
        }

        instance = new TrilaterationManager(algorithm);
    }

    return instance;
}

TrilaterationManager::TrilaterationManager(AbstractMultiAlgorithm* algorithm) {
    stationStorage = StationStorage::getInstance();
    environmentalStorage = EnvironmentalStorage::getInstance();

    this->algorithm = algorithm;

    logger->info("Trilateration manager successfully initiated");
}

void TrilaterationManager::find() {
    if (!stationStorage->isValid()) {
        return;
    }

    auto stations = stationStorage->getStations();

    algorithm->init(stations);

    stationStorage->invalidateStations();
}