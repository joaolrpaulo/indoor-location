#include <sstream>
#include "BrokerManager.h"
#include "../Networking/ZMQServer.h"

/**
 * BrokerManager
 *
 * The class responsible for delegating jobs for each emitter and receiver
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
BrokerManager* BrokerManager::instance = nullptr;

BrokerManager* BrokerManager::getInstance() {
    if (instance == nullptr) {
        instance = new BrokerManager();
    }

    return instance;
}

BrokerManager::BrokerManager() {
    logger->info("Broker manager successfully initiated");
}

std::thread BrokerManager::init() {
    configParser = ConfigParser::getInstance();

    return std::thread( [=] () -> void {
        privateInit();
    });
}

void BrokerManager::privateInit() {
    std::vector<std::thread*> threads;

    if (configParser->areGNURadioServicesActive()) {
        std::thread* pythonGNURadio = new std::thread( [=] {
            system(buildPythonScriptPath(configParser->getGnuradioSourceDir(), "indoorlocation_transmitter"));
        });

        threads.push_back(pythonGNURadio);

        pythonGNURadio = new std::thread( [=] {
            system(buildPythonScriptPath(configParser->getGnuradioSourceDir(), "indoorlocation_receiver"));
        });

        threads.push_back(pythonGNURadio);
    }


    ZMQServer* zmqServer = ZMQServer::getInstance(configParser->getHost());

    threads.push_back(zmqServer->init());

    for (auto thread: threads) {
        thread->join();
    }
}

const char * BrokerManager::buildPythonScriptPath(const std::string gnuradioSourceDir, const std::string scriptName) {
    std::stringstream pythonRunCommand;

    pythonRunCommand
            << "python -c \"import sys, os; sys.path.append(os.getcwd() + '"
            << gnuradioSourceDir
            << "'); import " << scriptName << "; " << scriptName << ".main();\"";

    return pythonRunCommand.str().c_str();
}
