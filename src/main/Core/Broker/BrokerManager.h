#ifndef INDOOR_LOCATION_BROKERMANAGER_H
#define INDOOR_LOCATION_BROKERMANAGER_H

#include <thread>
#include <spdlog/spdlog.h>
#include "../../Types/TCPHost.h"
#include "../../Config/ConfigParser.h"

class BrokerManager {
private:
    static BrokerManager* instance;
    BrokerManager();

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("BrokerManager");

    void privateInit();

    ConfigParser* configParser;

    const char * buildPythonScriptPath(std::string, std::string);
public:
    static BrokerManager* getInstance();

    std::thread init();
};

#endif //INDOOR_LOCATION_BROKERMANAGER_H
