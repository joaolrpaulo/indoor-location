#include <spdlog/spdlog.h>
#include "StationStorage.h"


/**
 * StationStorage
 *
 * The class responsible for storing Station data in-memory.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
StationStorage::StationStorage() {
    logger->info("Station storage successfully initiated");
}

StationStorage* StationStorage::instance = nullptr;

StationStorage* StationStorage::getInstance() {
    if (instance == nullptr) {
        instance = new StationStorage();
    }

    return instance;
}

void StationStorage::invalidateStation(const std::string &stationID) {

    auto it = std::find_if(stations.begin(), stations.end(), [stationID](const Station* obj) {
        return obj->getStationID() == stationID;
    });

    if (it != stations.end()) {
        it.operator*()->setDeltaTime(-1);
    }
}

void StationStorage::invalidateStations() {
    for (auto iter = stations.begin(); iter != stations.end(); ++iter) {
        iter.operator*()->setDeltaTime(-1);
    }
}

void StationStorage::insertOrUpdate(std::string stationID, Station* station, long delta_time) {
    auto it = std::find_if(stations.begin(), stations.end(), [&stationID](const Station* obj) {
        return obj->getStationID() == stationID;
    });

    if (it != stations.end()) {
        it.operator*()->setDeltaTime(delta_time);
    } else {
        stations.push_back(station);
    }
}

bool StationStorage::isValid() {
    std::vector<long> times;

    for (auto iter = stations.begin(); iter != stations.end(); ++iter) {
        times.push_back(iter.operator*()->getDeltaTime());
    }

    std::sort(times.begin(), times.end());

    // 15 ms
    return times[2] - times[0] < 100000;
}

std::vector<Station*> StationStorage::getStations() {
    return stations;
}
