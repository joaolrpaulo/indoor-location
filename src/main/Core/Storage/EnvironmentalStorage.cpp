#include <spdlog/spdlog.h>
#include "EnvironmentalStorage.h"

/**
 * EnvironmentalStorage
 *
 * The class responsible for storing in-memory data about the environment.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
EnvironmentalStorage* EnvironmentalStorage::instance = nullptr;

EnvironmentalStorage* EnvironmentalStorage::getInstance() {
    if (instance == nullptr) {
        instance = new EnvironmentalStorage();
    }

    return instance;
}

EnvironmentalStorage::EnvironmentalStorage() {
    /*
     * All these values are in meters per microsecond
     */
    speedOfSound["0"]  = 0.00033130;
    speedOfSound["5"]  = 0.00033432;
    speedOfSound["10"] = 0.00033731;
    speedOfSound["15"] = 0.00034027;
    speedOfSound["20"] = 0.00034321;
    speedOfSound["25"] = 0.00034613;
    speedOfSound["30"] = 0.00034902;
    speedOfSound["35"] = 0.00035188;

    logger->info("Environmental storage successfully initiated");
}

std::map<std::string, double> EnvironmentalStorage::getSpeedOfSound() {
    return speedOfSound;
}
