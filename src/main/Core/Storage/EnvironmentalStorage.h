#ifndef INDOOR_LOCATION_CPP_ENVIRONMENTALSTORAGE_H
#define INDOOR_LOCATION_CPP_ENVIRONMENTALSTORAGE_H

#include <map>
#include <string>
#include <spdlog/spdlog.h>

class EnvironmentalStorage {
private:
    static EnvironmentalStorage* instance;
    EnvironmentalStorage();

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("EnvironmentalStorage");

    std::map<std::string, double> speedOfSound;

public:
    static EnvironmentalStorage* getInstance();
    std::map<std::string, double> getSpeedOfSound();
};

#endif //INDOOR_LOCATION_CPP_ENVIRONMENTALSTORAGE_H
