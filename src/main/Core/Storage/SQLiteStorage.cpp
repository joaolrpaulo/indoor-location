//
// Created by joaopaulo on 11-11-2018.
//

#include <iostream>
#include "SQLiteStorage.h"
#include "../../Config/ConfigParser.h"

/**
 * SQLiteStorage
 *
 * Creates entries in the database related with the location.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
SQLiteStorage::SQLiteStorage() {
    int rc = sqlite3_open(ConfigParser::getInstance()->getDatabaseFilePath().c_str(), &db);

    if (rc) {
        logger->error("Can't open database: {}", sqlite3_errmsg(db));
    } else {
        logger->info("SQLite storage successfully initiated");
    }
}

SQLiteStorage* SQLiteStorage::instance = nullptr;

SQLiteStorage* SQLiteStorage::getInstance() {
    if (instance == nullptr) {
        instance = new SQLiteStorage();
    }

    return instance;
}

void SQLiteStorage::persistTrilaterationResult(__int64_t timestampNowMicro, long t1, long t2, long t3,
        long tdoa21, long tdoa23,
        double x, double y, double distance) {
    char *zErrMsg = nullptr;

    std::string pSQL =
            "INSERT INTO LOCATION VALUES ('"
            + std::to_string(timestampNowMicro) + "','"
            + std::to_string(t1) + "','" + std::to_string(t2) + "','" + std::to_string(t3) + "','"
            + std::to_string(tdoa21) + "','" + std::to_string(tdoa23) + "','"\
            + std::to_string(x) + "','" + std::to_string(y) + "','" + std::to_string(distance) + "')";

    int rc = sqlite3_exec(db, pSQL.c_str(), SQLiteStorage::callback, nullptr, &zErrMsg);

    if( rc != SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
}

int SQLiteStorage::callback(void*, int argc, char **argv, char **azColName) {
    for (int i = 0; i < argc; i++) {
        std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
    }
    std::cout << std::endl;

    return 0;
}
