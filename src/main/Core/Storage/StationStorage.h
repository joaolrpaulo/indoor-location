#ifndef INDOOR_LOCATION_CPP_STATIONSTORAGE_H
#define INDOOR_LOCATION_CPP_STATIONSTORAGE_H

#include <map>
#include <vector>
#include <spdlog/logger.h>
#include <spdlog/spdlog.h>
#include "../../Types/Station.h"

class StationStorage {
private:
    static StationStorage* instance;
    StationStorage();

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("StationStorage");

    std::vector<Station*> stations;
public:
    static StationStorage* getInstance();
    long received = 0;
    long sent = 0;

    void invalidateStation(const std::string &);
    void invalidateStations();
    void insertOrUpdate(std::string, Station*, long);

    bool isValid();
    std::vector<Station*> getStations();
};

#endif //INDOOR_LOCATION_CPP_STATIONSTORAGE_H
