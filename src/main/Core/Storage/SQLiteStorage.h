//
// Created by joaopaulo on 11-11-2018.
//

#ifndef INDOOR_LOCATION_SQLITESTORAGE_H
#define INDOOR_LOCATION_SQLITESTORAGE_H


#include <spdlog/logger.h>
#include <spdlog/spdlog.h>
#include <sqlite3.h>

class SQLiteStorage {
private:
    static SQLiteStorage* instance;
    SQLiteStorage();

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("SQLiteStorage");

    sqlite3 *db;
public:
    static SQLiteStorage* getInstance();


    static int callback (void *NotUsed, int argc, char **argv, char **azColName);
    void persistTrilaterationResult(__int64_t timestampNowMicro, long t1, long t2, long t3, long tdoa21, long tdoa23, double x, double y, double distance);
};


#endif //INDOOR_LOCATION_SQLITESTORAGE_H
