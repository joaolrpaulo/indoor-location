#ifndef INDOOR_LOCATION_ZMQSERVER_H
#define INDOOR_LOCATION_ZMQSERVER_H

#include <spdlog/spdlog.h>
#include <zmq.hpp>
#include "../../Types/TCPHost.h"

class ZMQServer {
private:
    static ZMQServer* instance;
    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("ZMQServer");

    TCPHost* tcpHost;

    zmq::context_t ctx_;
    zmq::socket_t frontend_;
    zmq::socket_t backend_;

    enum { kMaxThread = 1 };

    void privateInit();
public:
    static ZMQServer* getInstance(TCPHost*);

    explicit ZMQServer(TCPHost*);

    std::thread* init();
};

#endif //INDOOR_LOCATION_ZMQSERVER_H
