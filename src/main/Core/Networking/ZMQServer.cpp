#include <sstream>
#include "ZMQServer.h"
#include "ZMQWorker.h"

/**
 * ZMQ Server
 *
 * Class that creates a ZMQ server based
 * on TCP connection which is capable of
 * receiving and responding to requests
 * asynchronously.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
ZMQServer* ZMQServer::instance = nullptr;

ZMQServer* ZMQServer::getInstance(TCPHost* host) {
    if (instance == nullptr) {
        instance = new ZMQServer(host);
    }

    return instance;
}

std::thread* ZMQServer::init() {
    return new std::thread( [=] () -> void {
        privateInit();
    });
}

void ZMQServer::privateInit() {
    std::stringstream zmqHostAddress;
    zmqHostAddress << "tcp://" << tcpHost->getIP() << ":" << tcpHost->getPort();

    frontend_.bind(zmqHostAddress.str());
    backend_.bind("inproc://backend");

    std::vector<std::thread *> worker_thread;
    for (int i = 0; i < kMaxThread; ++i) {
        auto worker = new ZMQWorker(ctx_, ZMQ_DEALER);

        worker_thread.push_back(new std::thread(std::bind(&ZMQWorker::work, worker)));
    }

    try {
        zmq::proxy(frontend_, backend_, nullptr);
    }
    catch (std::exception &e) { }

    for (auto thread : worker_thread) {
        thread->detach();
    }
}

ZMQServer::ZMQServer(TCPHost * host) :
        ctx_(1),
        frontend_(ctx_, ZMQ_ROUTER),
        backend_(ctx_, ZMQ_DEALER),
        tcpHost(host) { }
