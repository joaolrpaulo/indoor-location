#ifndef INDOOR_LOCATION_ZMQWORKER_H
#define INDOOR_LOCATION_ZMQWORKER_H

#include <zmq.hpp>
#include <spdlog/spdlog.h>
#include "../Storage/StationStorage.h"
#include "../Trilateration/TrilaterationManager.h"

#define within(num) (int) ((float) (num) * random () / (RAND_MAX + 1.0))

class ZMQWorker {
private:
    std::shared_ptr<spdlog::logger> logger;

    zmq::context_t &ctx_;
    zmq::socket_t worker_;
    std::mutex m;
    std::map <std::string, long> mapTimes;

    StationStorage* stationStorage;
    TrilaterationManager* trilaterationManager;
public:
    ZMQWorker(zmq::context_t &ctx, int sock_type);

    void work();

    bool markClear;
};

#endif //INDOOR_LOCATION_ZMQWORKER_H
