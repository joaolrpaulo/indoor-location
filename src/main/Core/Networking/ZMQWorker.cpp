#include <sstream>
#include "ZMQWorker.h"

/**
 * ZMQWorker
 *
 * Abstraction of a worker so we can process
 * concurrent and thread safe ZMQ requests.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
void ZMQWorker::work() {
    worker_.connect("inproc://backend");

    try {
        while (true) {
            zmq::message_t identity;
            zmq::message_t msg;

            worker_.recv(&identity);
            worker_.recv(&msg);

            std::string identityStr = std::string(static_cast<char*>(identity.data()), identity.size());
            std::string msgStr = std::string(static_cast<char*>(msg.data()), msg.size());

            if (markClear && identityStr == "1") {
                //logger->info("Identity: {}", identityStr);
                markClear = false;
            } else if (markClear) {
                continue;
            }

            m.lock();
            auto ret = mapTimes.insert(std::pair<std::string, long>(identityStr, std::stol(msgStr)));
            //logger->info("Status: {}", msgStr, ret.second);

            if (!ret.second) {
                logger->error("Critical error");

                markClear = true;

                mapTimes.clear();
                m.unlock();
                continue;
            }

            if (mapTimes.size() == 3) {
                //logger->info("Locating...");

                for (auto &mapTime : mapTimes) {
                    stationStorage->insertOrUpdate(mapTime.first, nullptr, mapTime.second);
                }

                trilaterationManager->find();

                mapTimes.clear();
            }

            m.unlock();


            //logger->info("Sent: {}. Received: {}", stationStorage->sent, stationStorage->received);
        }
    }
    catch (std::exception &e) {
        // Add a fallback to restart the worker engine, or something better than just a logger.
        logger->critical("{}", e.what());
    }
}

ZMQWorker::ZMQWorker(zmq::context_t &ctx, int sock_type) :
        ctx_(ctx),
        worker_(ctx_, sock_type),
        stationStorage(StationStorage::getInstance()),
        trilaterationManager(TrilaterationManager::getInstance(nullptr)) {

    // Random identifier for each worker
    char identity[10] = {};
    sprintf(identity, "%04X-%04X", within(0x10000), within(0x10000));

    std::stringstream classLoggerName;
    classLoggerName << "ZMQWorker::" << identity;

    markClear = false;

    logger = spdlog::stdout_logger_mt(classLoggerName.str());
}