#ifndef INDOOR_LOCATION_CONFIGPARSER_H
#define INDOOR_LOCATION_CONFIGPARSER_H

#include <spdlog/spdlog.h>
#include "../Core/Storage/StationStorage.h"
#include "../Core/Storage/SQLiteStorage.h"

class ConfigParser {
private:
    static ConfigParser* instance;
    ConfigParser();

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("ConfigParser");
    StationStorage* stationStorage;
    TCPHost* receiverHost;
    std::string gnuradioSourceDir;
    std::string databaseFilePath;
    bool exponentialMovingAverage;
    bool shouldStartGNURadioServices;
public:
    static ConfigParser* getInstance();

    std::string getGnuradioSourceDir();
    std::string getDatabaseFilePath();
    TCPHost* getHost();
    bool isExponentialMovingAverageActive();
    bool areGNURadioServicesActive();
};

#endif //INDOOR_LOCATION_CONFIGPARSER_H
