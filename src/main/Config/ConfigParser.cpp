#include "ConfigParser.h"
#include <nlohmann/json.hpp>
#include <fstream>

using json = nlohmann::json;

/**
 * ConfigParser
 *
 * Class that load configuration into memory based
 * on properties present on the configuration JSON.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
ConfigParser* ConfigParser::instance = nullptr;

ConfigParser* ConfigParser::getInstance() {
    if (instance == nullptr) {
        instance = new ConfigParser();
    }

    return instance;
}

ConfigParser::ConfigParser() {
    json properties;

    std::ifstream inFile("properties.json");
    inFile >> properties;

    stationStorage = StationStorage::getInstance();

    for (const auto &station : properties["stations"]) {
        stationStorage->insertOrUpdate(
                station["id"],
                new Station(station["id"],station["position_X"], station["position_Y"]),
                0
        );
    }

    receiverHost = new TCPHost(properties["host"]["ip"], properties["host"]["port"]);

    gnuradioSourceDir = std::move(properties["gnuradio_source_dir"]);

    databaseFilePath = std::move(properties["database_file"]);

    exponentialMovingAverage = properties["exponentialMovingAverage"];

    shouldStartGNURadioServices = properties["shouldStartGNURadioServices"];

    logger->info("Configs successfully imported");
}

std::string ConfigParser::getGnuradioSourceDir() {
    return gnuradioSourceDir;
}

TCPHost* ConfigParser::getHost() {
    return receiverHost;
}

bool ConfigParser::isExponentialMovingAverageActive() {
    return exponentialMovingAverage;
}

bool ConfigParser::areGNURadioServicesActive() {
    return shouldStartGNURadioServices;
}

std::string ConfigParser::getDatabaseFilePath() {
    return databaseFilePath;
}
