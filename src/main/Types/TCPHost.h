#ifndef INDOOR_LOCATION_TCPHOST_H
#define INDOOR_LOCATION_TCPHOST_H

class TCPHost {
private:
    std::string ip;
    unsigned short port;

public:
    TCPHost(std::string, unsigned short);

    std::string getIP();
    unsigned short getPort();
};

#endif //INDOOR_LOCATION_TCPHOST_H
