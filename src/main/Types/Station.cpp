#include "Station.h"

/**
 * Station
 *
 * The class that represents the information
 * that is needed for each station
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
Station::Station(std::string station, double x, double y) {
    this->id = std::move(station);
    this->position = new Point(x, y);
    this->delta_time = -1;
}

std::string Station::getStationID() const {
    return id;
}

Point *Station::getPosition() {
    return position;
}

long Station::getDeltaTime() {
    return this->delta_time;
}

void Station::setDeltaTime(long delta_time) {
    this->delta_time = delta_time;
}
