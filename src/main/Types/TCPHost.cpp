#include <string>
#include "TCPHost.h"

/**
 * ConfigParser
 *
 * Represents the information loaded about
 * a TCP Host that is managed by the ZMQ Server.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
TCPHost::TCPHost(std::string stationIP, unsigned short stationPort) {
    this->ip = std::move(stationIP);
    this->port = stationPort;
}

std::string TCPHost::getIP() {
    return ip;
}

unsigned short TCPHost::getPort() {
    return port;
}