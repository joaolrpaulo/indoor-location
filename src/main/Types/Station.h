#ifndef INDOOR_LOCATION_CPP_STATION_H
#define INDOOR_LOCATION_CPP_STATION_H

#include <string>
#include "TCPHost.h"
#include "Point.h"

class Station {
private:
    std::string id;
    Point *position;
    long delta_time;

public:
    Station(std::string, double, double);

    long getDeltaTime();
    void setDeltaTime(long);

    std::string getStationID() const;
    Point* getPosition();
};

#endif //INDOOR_LOCATION_CPP_STATION_H
