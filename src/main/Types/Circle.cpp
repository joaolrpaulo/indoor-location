#include "Circle.h"

/**
 * Circle
 *
 * Represents the information about a Circle in a 2D space.
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
Circle::Circle(Point* p, double r) {
    this->p = p;
    this->r = r;
}

double Circle::getX() {
    return p->getX();
}

double Circle::getY() {
    return p->getY();
}

double Circle::getRadius() {
    return r;
}