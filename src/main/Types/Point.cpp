#include "Point.h"

/**
 * Point
 *
 * Represents a generic Point in a 2D Space
 *
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
Point::Point(double x, double y) {
    this->x = x;
    this->y = y;
}

double Point::getX() {
    return x;
}

double Point::getY() {
    return y;
}