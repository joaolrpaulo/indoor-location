#ifndef INDOOR_LOCATION_CPP_POINT_H
#define INDOOR_LOCATION_CPP_POINT_H

class Point {
private:
    double x;
    double y;

public:
    Point(double, double);
    double getX();
    double getY();
};

#endif //INDOOR_LOCATION_CPP_POINT_H
