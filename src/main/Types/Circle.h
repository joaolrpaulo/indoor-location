#ifndef INDOOR_LOCATION_CPP_CIRCLE_H
#define INDOOR_LOCATION_CPP_CIRCLE_H

#include "Point.h"

class Circle {
private:
    Point* p;
    double r;

public:
    Circle(Point*, double);
    double getX();
    double getY();
    double getRadius();
};

#endif //INDOOR_LOCATION_CPP_CIRCLE_H
