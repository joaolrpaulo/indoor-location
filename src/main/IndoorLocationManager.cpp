#include <iostream>
#include <csignal>

#include "Core/Broker/BrokerManager.h"
#include "Config/ConfigParser.h"

/**
 * IndoorLocationManager
 *
 * The main class that is responsible for starting all the services
 *
 * @param argc The arg count
 * @param argv The arg parameters
 * @author João Paulo (joaolrpaulo@gmail.com)
 */
int main(int argc, char** argv) {
    spdlog::set_pattern("%L - %T.%e \t [%P:%t] %n - %v");
    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("IndoorLocationManager");

    logger->info("Starting the indoor location service...");

    // Catch the CTRL-C handler and force stop all the threads
    signal (SIGINT, exit);

    BrokerManager* brokerManager = BrokerManager::getInstance();
    std::thread brokerThread = brokerManager->init();

    SQLiteStorage::getInstance();

    brokerThread.join();

    return 0;
}