from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np
import csv

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

with open("logRight3DDispersion.csv", "rb") as file:
    reader = csv.reader(file, delimiter=",")

    next(reader, None)

    tX = []
    tY = []
    tZ = []

    for row in reader:
        if len(row) > 10 and row[9] != "":
            tX.append([float(row[9])])
            tY.append([float(row[10])])
            tZ.append([float(row[8])])

    X, Y, Z = (np.array(tX), np.array(tY), np.array(tZ))

    # Plot a basic wireframe.
    # ax.scatter(X, Y, Z, marker='o')
    ax.plot_wireframe(X, Y, Z)

    plt.show()
 
