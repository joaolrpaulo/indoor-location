# Right Setup

Como podemos analisar pelo output do programa para calcular os percentis, podemos reparar que:

Ignored: 18681
Total: 39994
Ratio: 0.467095
Percentile 0: 0.299888
Percentile 10: 77.615012
Percentile 20: 100.577997
Percentile 30: 118.023527
Percentile 40: 132.599437
Percentile 50: 145.853600
Percentile 60: 158.354231
Percentile 70: 170.748373
Percentile 80: 183.082305
Percentile 90: 202.550799
Percentile 100: 3020.920600

Cerca de 47% dos dados foram ignorados porque não continham solução para o problema.
E que 90% dos dados estão contidos entre 0 e 202 cm de erro.

Todo este dataset foi gerado com diferenças de tempo entre 0 microsegundos e 10 milisegundos, com periodos de 100 microsegundos.

Pelo grafico gerado (Plot1.png e Plot2.png) podemos ver que temos alguns valores estranhos que distancia absurdas (percentil 90), e que o resto dos dados validos corresponde a erro baixo. Podemos tambem analisar que o erro aumenta consoante nos afastamos dos eixos X e Y. As zonas vazias no plano são os pontos para quais não foi possivel encontrar solução.