# Real Data

Pelos dados recolhidos (524 amostras), podemos verificar que cerca de 30% das mesmas (162 amostras) tem erros tão grandes que não existe solução real para a localização.

Tambem podemos reparar que os 10-percentiles seguem a seguinte distribuição:

Percentile 0: 7.948390
Percentile 10: 41.952854
Percentile 20: 68.115639
Percentile 30: 93.603581
Percentile 40: 109.910699
Percentile 50: 129.488066
Percentile 60: 151.093574
Percentile 70: 173.915426
Percentile 80: 230.813692
Percentile 90: 307.252718
Percentile 100: 813.306338

Com isto podemos ver que dentro dos restantes 70% dos dados, 50% tem erros maiores que 130cm.
Tambem podemos reparar que estes erros são imprevisiveis e derivam do problema de o GNURadio usar multiplas threads para efetuar o processamento.

Uma solução para este problema seria usar algum tipo de sincronização / datação dos samples recebidos antes de entrar num contexto multi-threaded no GNURadio. Para tal poderia-se usar blocos do tipo tagged_stream.