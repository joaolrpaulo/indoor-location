import math
import csv
import numpy as np
from operator import itemgetter

def main():
    with open("logRealData500SamplesClean.csv", "rb") as file:
        reader = csv.reader(file, delimiter=",")

        next(reader, None)

        data = []

        for row in reader:
            if row[1] == "":
                continue
            
            data.append(float(row[6].replace(",", ".")))

        for index in range(0, 110, 10):
            print "Percentile %d: %f" % (index, np.percentile(data, index))

if __name__ == "__main__":
    main()