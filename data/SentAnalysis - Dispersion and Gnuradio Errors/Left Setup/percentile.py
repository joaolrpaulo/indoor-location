import math
import csv
import numpy as np
from operator import itemgetter

def main():
    with open("logLeft3DDispersion.csv", "rb") as file:
        reader = csv.reader(file, delimiter=",")

        next(reader, None)

        data = []
        ignored = 0
        total = 0

        for row in reader:
            if row[1] == "":
                continue

            total+=1

            if len(row) <= 8:
                ignored+=1
                continue
            
            data.append(float(row[8].replace(",", ".")))

        print "Ignored: %d" % ignored
        print "Total: %d" % total
        print "Ratio: %f" % (ignored / float(total))
        for index in range(0, 110, 10):
            print "Percentile %d: %f" % (index, np.percentile(data, index))

if __name__ == "__main__":
    main()