# Left Setup

Como podemos analisar pelo output do programa para calcular os percentis, podemos reparar que:

Ignored: 19788
Total: 39995
Ratio: 0.494762
Percentile 0: 0.216000
Percentile 10: 70.983963
Percentile 20: 92.293966
Percentile 30: 109.151261
Percentile 40: 123.438242
Percentile 50: 135.897365
Percentile 60: 148.069378
Percentile 70: 159.381461
Percentile 80: 171.093367
Percentile 90: 183.099601
Percentile 100: 4895.790000

Cerca de 49% dos dados foram ignorados porque não continham solução para o problema.
E que 90% dos dados estão contidos entre 0 e 183 cm de erro.

Todo este dataset foi gerado com diferenças de tempo entre 0 microsegundos e 10 milisegundos, com periodos de 100 microsegundos.

Pelo grafico gerado (Plot1.png e Plot2.png) podemos ver que temos alguns valores estranhos que distancia absurdas (percentil 90), e que o resto dos dados validos corresponde a erro baixo. Podemos tambem analisar que o erro aumenta consoante nos afastamos dos eixos X e Y. As zonas vazias no plano são os pontos para quais não foi possivel encontrar solução.

Podemos reparar que os dados obtidos entre esta distribuição e a distribuição para o setup do simetrico em relação ao eixo do X (Right Setup), foram bastante proximas e podemos concluir que os dados reais seguiriam tambem essa propriedade.