import csv
import math

def main():
    
    header = []
    newArray = []
    newArray2 = []

    with open("logRight3DDispersion.csv", "rb") as file:
        reader = csv.reader(file, delimiter=",")

        addedRowCount = 0
        splitAt = 4

        for index, row in enumerate(reader):
            if index == 0:
                row.append("disReal")
                row.append("baset1")
                row.append("baset2")
                row.append("baset3")
                header = row
                continue

            if not "nan" in row[6]:
                x21 = (float(row[5]) - 65)**2
                y21 = (float(row[6]) - 190)**2
                c = math.sqrt(x21 + y21)

                row.append(c)
                row.append(int(row[0]) - 1539900448006651)
                row.append(int(row[1]) - 1539900448005801)
                row.append(int(row[2]) - 1539900448005489)

            newArray.append(row)

        for row in chunks(newArray, 4):
            for elem in row:
                newArray2.append(elem)

            newArray2.append(["","","","","","","","","","","",""])

        
    with open("logRight3DDispersionReady.csv", 'a') as outcsv:   
        #configure writer to write standard csv file
        writer = csv.writer(outcsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        writer.writerow(header)

        for item in newArray2:
            #Write item to outcsv
            writer.writerow(item)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

if __name__ == "__main__":
    main()