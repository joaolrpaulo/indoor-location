from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import csv
import pandas as pd

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')

with open("logCenter3DDispersion.csv", "rb") as file:
    reader = csv.reader(file, delimiter=",")

    next(reader, None)

    tX = []
    tY = []
    tZ = []

    for row in reader:
        if len(row) == 12 and row[9] != "":
            tX.append(int(row[9]))
            tY.append(int(row[11]))
            tZ.append(float(row[8]))


    df = pd.DataFrame({ 'x': tX, 'y': tY, 'z': tZ })

    fig = plt.figure()
    ax = Axes3D(fig)

    surf = ax.plot_trisurf(df.x, df.y, df.z, cmap=cm.jet, linewidth=0.1)

    ax.set_xlabel('X axis')
    ax.set_ylabel('Y axis')
    ax.set_zlabel('Z axis')

    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()

    # X, Y, Z = (np.array(tX), np.array(tY), np.array(tZ))

    # # Plot a basic wireframe.
    # # ax.scatter(X, Y, Z, marker='o')
    # ax.plot_wireframe(X, Y, Z)

    # plt.show()
 